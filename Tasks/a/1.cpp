// 1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

int main()
{
	unsigned int n;

	cout << "Please,enter n - natural: ";
	cin >> n;

	unsigned int a{ 1 };
	double sum{ 0 };

	while (a <= n)
	{
		sum += 1. / a;
		a = ++a;
	}

	cout << sum << endl;

	return 0;
}