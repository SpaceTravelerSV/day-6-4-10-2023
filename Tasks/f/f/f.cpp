// f.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream> <cmath>

using namespace std;

int main()
{
	unsigned int n;

	cout << "Please,enter n - natural: ";
	cin >> n;

	int a{ 1 };
	double sum{ 0 };

	while (a <= n)
	{
		sum += pow(-1.,a) / (2 * a + 1);
		a = ++a;
	}

	cout << sum << endl;

	return 0;
}

