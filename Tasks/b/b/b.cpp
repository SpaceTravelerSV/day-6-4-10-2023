// b.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream> <cmath>

using namespace std;

int main()
{
	unsigned int n;

	cout << "Please,enter n - natural: ";
	cin >> n;

	unsigned int a{ 1 };
	double sum{ 0 };
	double quot{ 1 };

	while (a <= n)
	{
		sum = 1 + (1. / pow(a, 2));
		a = ++a;
		quot *= sum;
	}

	cout << quot << endl;

	return 0;
}

