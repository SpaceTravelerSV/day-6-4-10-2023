// 5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream> <cmath> 
using namespace std;

int main()
{
	unsigned int n;

	cout << "Please,enter n - natural: ";
	cin >> n;

	unsigned int a{ 1 };
	double sum{ 0 };

	while (a<=n)
	{
		sum += 1. / pow(2 * a + 1, 2);
		a = ++a;
	}

	cout << sum << endl;

	return 0;
}

